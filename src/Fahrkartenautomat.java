
// smeya, FI-C 12
import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;

		do {
			zuZahlenderBetrag = BestellungErfassen();
			eingezahlterGesamtbetrag = FahrkartenBezahlen(zuZahlenderBetrag);

			FahrkartenAusgeben();
			RueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);

			warte(1500);
		} while (true);
	}

	public static double BestellungErfassen() {

		String[] Fahrkarten_Namen = { "Einzelfahrschein AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte BC",
				"Tageskarte ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };
		double[] Fahrkarten_Preise = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
		int[] Fahrkarten_Nummer = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		Scanner myScanner = new Scanner(System.in);
		double zuZahlenderBetrag = 0;
		int Fahrkarten;
		int Auswahl;

		System.out.println("Auswahlnummer     Bezeichnung                          Preis in EURO:");

		for (int i = 0; i <= 9; i++) {
			System.out.println(Fahrkarten_Nummer[i] + "                 " + Fahrkarten_Namen[i] + "                        " + Fahrkarten_Preise[i]);
		}

		Auswahl = myScanner.nextInt();

		System.out.println("Ihre Wahl: " + Auswahl);

		System.out.print("Anzahl der Fahrkarten: ");
		Fahrkarten = myScanner.nextInt();

		switch (Auswahl) {
		case 1:
			zuZahlenderBetrag = Fahrkarten_Preise[0] * Fahrkarten;
			break;
		case 2:
			zuZahlenderBetrag = Fahrkarten_Preise[1] * Fahrkarten;
			break;
		case 3:
			zuZahlenderBetrag = Fahrkarten_Preise[2] * Fahrkarten;
			break;
		case 4:
			zuZahlenderBetrag = Fahrkarten_Preise[3] * Fahrkarten;
			break;
		case 5:
			zuZahlenderBetrag = Fahrkarten_Preise[4] * Fahrkarten;
			break;
		case 6:
			zuZahlenderBetrag = Fahrkarten_Preise[5] * Fahrkarten;
			break;
		case 7:
			zuZahlenderBetrag = Fahrkarten_Preise[6] * Fahrkarten;
			break;
		case 8:
	        zuZahlenderBetrag = Fahrkarten_Preise[7] * Fahrkarten;
	        break;
		case 9:
	        zuZahlenderBetrag = Fahrkarten_Preise[8] * Fahrkarten;
	        break;
		case 10:
	        zuZahlenderBetrag = Fahrkarten_Preise[9] * Fahrkarten;
	        break;
		}

		return zuZahlenderBetrag;
	}

	public static double FahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner myScanner = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("Noch zu zahlender Betrag in Euro:\n");

			System.out.println(zuZahlenderBetrag - eingezahlterGesamtbetrag);

			System.out.println("Eingabe (mindestens 5 Cent, höchstens 2 Euro): ");

			eingeworfeneMünze = myScanner.nextDouble();

			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void FahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 25; i++) {
			System.out.print("=");
			warte(125);
		}

		System.out.println("\n");

	}

	public static void RueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
		double Rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (Rueckgabebetrag > 0.0) {
			System.out.println(
					"Der Rückgabebetrag in Höhe von " + Rueckgabebetrag + " EURO wird in folgenden Münzen ausgezahlt:");

			while (Rueckgabebetrag >= 2.0) {
				System.out.println("2 EURO");
				Rueckgabebetrag -= 2.0;
				Rueckgabebetrag = Math.round(Rueckgabebetrag * 100) / 100.0;
			}
			while (Rueckgabebetrag >= 1.0) {
				System.out.println("1 EURO");
				Rueckgabebetrag -= 1.0;
				Rueckgabebetrag = Math.round(Rueckgabebetrag * 100) / 100.0;
			}
			while (Rueckgabebetrag >= 0.5) {
				System.out.println("50 CENT");
				Rueckgabebetrag -= 0.5;
				Rueckgabebetrag = Math.round(Rueckgabebetrag * 100) / 100.0;
			}
			while (Rueckgabebetrag >= 0.2) {
				System.out.println("20 CENT");
				Rueckgabebetrag -= 0.2;
				Rueckgabebetrag = Math.round(Rueckgabebetrag * 100) / 100.0;
			}
			while (Rueckgabebetrag >= 0.1) {
				System.out.println("10 CENT");
				Rueckgabebetrag -= 0.1;
				Rueckgabebetrag = Math.round(Rueckgabebetrag * 100) / 100.0;
			}
			while (Rueckgabebetrag >= 0.05) {
				System.out.println("5 CENT");
				Rueckgabebetrag -= 0.05;
				Rueckgabebetrag = Math.round(Rueckgabebetrag * 100) / 100.0;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n" + "______________________________________\n\n");
	}

	public static void warte(int milisecond) {
		try {
			Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}